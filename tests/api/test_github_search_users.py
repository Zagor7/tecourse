def test_search_users_positive(fixture_git_hub_api_client):
    name_of_user = "coder"

    list_of_users = fixture_git_hub_api_client.search_users(name_of_user)
    print(list_of_users)
    assert name_of_user in list_of_users
