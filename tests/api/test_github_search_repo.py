def test_search_repo_positive(fixture_git_hub_api_client):
    name_of_repo = 'python'

    list_of_repos = fixture_git_hub_api_client.search_repo(name_of_repo)

    assert name_of_repo in list_of_repos


def test_search_repo_negative(fixture_git_hub_api_client):
    name_of_repo = 'asdfg04fj2309fjq34fj3q9fj3q98jf8934jf'

    list_of_repos = fixture_git_hub_api_client.search_repo(name_of_repo)

    assert name_of_repo not in list_of_repos


def test_search_milions_results(fixture_git_hub_api_client):
    name_of_repo = "a"
    per_page = 100

    list_of_repos = fixture_git_hub_api_client.search_repo(name_of_repo, per_page)
    print(len(list_of_repos))
    assert len(list_of_repos) == per_page

