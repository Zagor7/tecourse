from src.config.config import Config
import requests


class GitHubAPIClient:

    # def __call__(self, *args: Any, **kwds: Any) -> Any:
    #     pass

    # def __new__(cls) -> Self:
    #     pass

    def __init__(self) -> None:
        pass

    def login(self, username, password):
        print(f"DO LOGIN for {username} and {password}")

    def logout(self):
        print("DO LOGOUT")

    def search_repo(self, name_of_repo, per_page=50):
        r = requests.get(
            url=f"{Config.get_property('API_BASE_URL')}/search/repositories",
            headers={
                "Accept": "application/vnd.github+json",
                "X-GitHub-Api-Version": "2022-11-28",
            },
            params={
                'q': name_of_repo,
                'per_page': per_page
            },
        )
        print("Get Search Repo Response Status Code:", r.status_code)

        r.raise_for_status()

        body = r.json()

        body_repo_names = [x['name'] for x in body['items']]

        return body_repo_names

    def get_emojis(self):
        """
            Get list of available emojis in github sustem
        """

        r = requests.get(
            url=f"{Config.get_property('API_BASE_URL')}/emojis",
            headers={
                "Accept": "application/vnd.github+json",
                "X-GitHub-Api-Version": "2022-11-28",
            }
        )
        print("Get Emojis Response Status Code:", r.status_code)

        r.raise_for_status()
        body = r.json()
        list_of_emojis = body.keys()
        return list_of_emojis

    def search_users(self, name_of_user):
        r = requests.get(
            url=f"{Config.get_property('API_BASE_URL')}/search/users",
            headers={
                "Accept": "application/vnd.github+json",
                "X-GitHub-Api-Version": "2022-11-28",
            },
            params={
                'q': name_of_user,
            },
        )
        print("Get Search Users Response Status Code:", r.status_code)

        r.raise_for_status()

        body = r.json()

        body_user_names = [x['login'] for x in body['items']]

        return body_user_names
